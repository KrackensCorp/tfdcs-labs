#include <cstdio>
#include <cstdlib>
#include <iomanip> 
#include <iostream>
#include <vector>

using namespace std;

double teta(double lambda, double mu, int n, int N, int m) {
	double sum = 0.0f;
	double multipy = 1.0f;
	double mu_l;
	
	for (int j = n + 1; j <= N; j++) {
		double l = j - 1;
		if (l >= N - m && l <= N) {
			mu_l = ((N - l) * mu);
		} else {
			mu_l = m * mu;
		}
		multipy *= mu_l / (l * lambda);
		sum += multipy / (j * lambda);
	}
	return sum + 1 / (n * lambda);
}

double t(double lambda, double mu, int n, int N, int m) {
	double mu_l;
	if (n == 1) {
		return m * mu;
	}
	double multipy = 1.0f;
	for (int l = 1; l <= n - 1; l++) {
		multipy *= (l * lambda) / (mu * l);
	}

	double sum = 0.0f;
	for (int j = 1; j <= n - 1; j++) {
		double multipy = 1.0f;
		for (int l = j; l <= n - 1; l++) {
			if (l >= N - m && l <= N) {
				mu_l = ((N - l) * mu);
			} else {
				mu_l = m * mu;
			}
			multipy *= l * lambda / mu_l;
		}
		sum += multipy / (j * lambda);
	}
	return multipy + sum;
}

int main()  {
	vector<double> lambda_vec = {1e-5, 1e-6, 1e-7};
	double res_Teta, res_T;
	int N = 65536;
	
	for (double lambda : lambda_vec) {
		for (int mu = 1; mu <= 1000; mu *= 10) {
			for (int m = 1; m <= 3; m++) {
				for (int n = 65527; n <= 65536; n++) {
					cout << lambda << ";" << mu << ";" << m << ";" << n << ";";
					
					res_Teta = teta(lambda, mu, n, N, m);
					cout << res_Teta;

					cout << ";";

					res_T = t(lambda, mu, n, N, m);
					cout << res_T;

					cout << endl;
				}
			}
		}
	}
	return 0;
}