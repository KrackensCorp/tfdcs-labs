#include "function.h"

double U_func(int n, int N, int m, double mu, double lambda, int t);
double P(int j, double mu, double lambda, int N);
double Pi(int t, double lambda, int r, int i);
double U(double mu, int t, int N, int m, int l, int i);
double Q(int i, int n, int t, int m, int N, double mu, double lambda);
double R(int n, int t, int m, int N, double mu, double lambda);

double lambda = 0.024, mu = 0.71;

int factorial(int n) {
    if (n <= 1)
        return 1;
    return n * factorial(n - 1);
}

int delta(int x) {
    return (x >= 0) ? 1 : 0;
}

double P(int j, double mu, double lambda, int N) {
    double sum = 0;

    for (int l = 0; l <= N; l++)
        sum += pow(mu / lambda, l) / factorial(l);

    sum = 1 / sum;
    sum *= pow(mu / lambda, j) / factorial(j);

    return sum;
}

double Pi(int t, double lambda, int r, int i) {
    return pow(i * lambda * t, r) / factorial(r) * exp(-i * lambda * t);
}

double U(double mu, int t, int N, int m, int l, int i) {
    double u = pow(mu * t, l) / factorial(l);

    u *= delta(N - i - m) * pow(m, l) * exp(-m * mu * t) + delta(m - N + i) * pow(N - i, l) * exp(-(N - i) * mu * t);

    return u;
}

double Q(int i, int n, int t, int m, int N, double mu, double lambda) {
    double sum_U = 0;
    double sum = 0;

    for (int l = 0; l <= 1; l++) {
        sum_U = U(mu, t, N, m, l, i);
        double sum_Pi = 0;
        for (int r = 0; r <= i - n + l; r++)
            sum_Pi += Pi(t, lambda, r, i);
        sum += sum_U * sum_Pi;
    }

    return sum;
}

double R(int n, int t, int m, int N, double mu, double lambda) {
    double sum = 0;
    for (int i = n; i <= N; i++)
        sum += P(i, mu, lambda, N) * Q(i, n, t, m, N, mu, lambda);

    return sum;
}

double S(int n, int m, double mu, double lambda) {
    double sum = 0;
    for (int j = 0; j <= n - 1; j++)
        sum += pow(m * mu / lambda, j) * exp(-m * mu / lambda) / factorial(j);

    return 1 - sum;
}

double number1() {
    int N = 10, m = 1;

    for (int n = 8; n <= 10; n++) {
        cout << setw(0) << n << ";" << setw(15);
        for (int t = 0; t <= 24; t += 2)
            cout << R(n, t, m, N, mu, lambda) << ";" << setw(15);
        cout << endl;
    }

    return 0;
}

double U_func(int n, int N, int m, double mu, double lambda, int t) {
    double sum_P = 0;

    for (int i = 0; i <= n - 1; i++) {
        double sum_Pi = 0;
        for (int r = 0; r <= 1; r++) {
            double sum_U = 0;
            for (int l = 0; l <= n - i - 1 + r; l++)
                sum_U += U(mu, t, N, m, l, i);
            sum_Pi += Pi(t, lambda, r, i) * sum_U;
        }
        sum_P += P(i, mu, lambda, N) * sum_Pi;
    }

    return 1 - sum_P;
}

double number2() {
    int N = 16, m = 1;

    for (int n = 10; n <= N; ++n) {
        cout << setw(0) << n << ";" << setw(15);
        for (int t = 0; t <= 24; t += 2)
            cout << U_func(n, N, m, mu, lambda, t) << ";" << setw(15);
        cout << endl;
    }

    return 0;
}

double number3() {

    for (int n = 11; n <= 16; n++) {
        cout << setw(0) << n << ";" << setw(15);
        for (int m = 1; m <= 16; m += 15)
            cout << S(n, m, mu, lambda) << ";" << setw(10);
        cout << endl;
    }

    return 0;
}

int main(int argc, char *argv[]) {
    int choice;

    cout << "\n1. N = 10, n \u2208 {8, 9, 10}, m = 1, \u03BB = 0.024 1/ч, \u03BC = 0.71 1/ч, t = 0, 2, 4, ..., 24 ч\n";
    cout << "2. N = 16, n \u2208 {10, 11, ..., 16}, m = 1, \u03BB = 0.024 1/ч, \u03BC = 0.71 1/ч, t = 0, 2, 4, ..., 24 ч.\n";
    cout << "3. N = 16, \u03BB = 0.024 1/ч, \u03BC = 0.71 1/ч\n";
    cout << "Select: ";

    cin >> choice;

    if (choice == 1) return number1();
    if (choice == 2) return number2();
    if (choice == 3) return number3();
}
