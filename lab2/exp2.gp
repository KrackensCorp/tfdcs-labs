set terminal pdf color enhanced font 'Times,14' size 19cm,12cm
set bmargin 5.2
set output 'graphic2.pdf'
set key outside right top

#set colorsequence default|podo|classic
set colorsequence classic

set key outside right top
set title "N = 16, λ = 0.024, mu = 0.71, m = 1"
set style line 1 lc rgb 'blue' lw 1 pt 5 ps 0.5
set style line 2 lt 1 lw 1 pt 2 ps 0.5
set style line 3 lt 2 lw 1 pt 3 ps 0.5
set style line 4 lt 4 lw 1 pt 5 ps 0.5
set style line 5 lt 5 lw 1 pt 7 ps 0.5
set style line 6 lt 6 lw 1 pt 9 ps 0.5
set style line 7 lt 7 lw 1 pt 13 ps 0.5
set style line 8 lt 8 lw 1 pt 3 ps 0.5
set style line 9 lt 9 lw 1 pt 5 ps 0.5
set style line 10 lt 10 lw 1 pt 7 ps 0.5
set style line 11 lt 11 lw 1 pt 9 ps 0.5
set style line 12 lt 12 lw 1 pt 13 ps 0.5
set style line 13 lt 13 lw 1 pt 3 ps 0.5
set style line 14 lt 14 lw 1 pt 5 ps 0.5
set style line 15 lt 15 lw 1 pt 7 ps 0.5
set style line 16 lt 16 lw 1 pt 9 ps 0.5

set xlabel "Time, hours" font 'Times,18'
set ylabel "Operational recoverability" font 'Times,18'

set yrange [0:1.2]

set xrange [0:24]
set xtics (0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24)

set grid xtics ytics

plot 'exp2' using 1:2 title "n = 10" with linespoints ls 1, \
     'exp2' using 1:3 title "n = 11" with linespoints ls 2, \
     'exp2' using 1:4 title "n = 12" with linespoints ls 3, \
     'exp2' using 1:5 title "n = 13" with linespoints ls 4, \
     'exp2' using 1:6 title "n = 14" with linespoints ls 5, \
     'exp2' using 1:7 title "n = 15" with linespoints ls 6, \
     'exp2' using 1:8 title "n = 16" with linespoints ls 7,
