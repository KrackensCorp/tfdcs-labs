import kotlin.math.exp
import kotlin.math.pow

fun main() {
    tast1()
    println()
    tast2()
    println()
    task3()
}

private fun tast1() {
    val N = 10.0
    val n = doubleArrayOf(8.0, 9.0, 10.0)
    val lya = 0.024
    val mu = 0.71
    val m = doubleArrayOf(1.0)
    val t =
        doubleArrayOf(0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0)
    var J = 0
    val res = Array(t.size) {
        DoubleArray(
            n.size
        )
    }
    for ((I, t_) in t.withIndex()) {
        for (n_ in n) {
            for (m_ in m) {
                res[I][J] = R_(lya, mu, m_, n_, N, t_)
            }
            J++
        }
        J = 0
    }
    soutLength(t, n, res)
}

private fun tast2() {
    val N = 16.0
    val n = doubleArrayOf(10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0)
    val lya = 0.024
    val mu = 0.71
    val m = doubleArrayOf(1.0)
    val t =
        doubleArrayOf(0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0)
    var J = 0
    val res = Array(t.size) {
        DoubleArray(
            n.size
        )
    }
    for ((I, t_) in t.withIndex()) {
        for (n_ in n) {
            for (m_ in m) {
                res[I][J] = U_(lya, mu, m_, n_, N, t_)
            }
            J++
        }
        J = 0
    }
    soutLength(t, n, res)
}

private fun task3() {
    val N = 16.0
    val n = doubleArrayOf(11.0, 12.0, 13.0, 14.0, 15.0, 16.0)
    val lya = 0.024
    val mu = 0.71
    val m = doubleArrayOf(1.0, 16.0)
    var J = 0
    val res = Array(n.size) {
        DoubleArray(
            m.size
        )
    }
    for ((I, n_) in n.withIndex()) {
        for (m_ in m) {
            res[I][J] = S(lya, mu, m_, n_, N, 0.0)
            J++
        }
        J = 0
    }
    soutLength(n, m, res)
}

private fun soutLength(n: DoubleArray, m: DoubleArray, res: Array<DoubleArray>) {
    for (i in n.indices) {
        print(n[i].toString() + "\t")
        for (j in m.indices) {
            print(res[i][j].toString() + "\t")
        }
        println()
    }
}

fun R_(lya: Double, mu: Double, m: Double, n: Double, N: Double, t: Double): Double {
    var buf = 0.0
    return if (t == 0.0) {
        S(lya, mu, m, n, N, t)
    } else {
        var i = n
        var prod = 0.0
        while (i <= N) {
            var r = 0.0
            while (r <= i - n) {
                prod += P_ir(lya, mu, m, n, N, t, r, i)
                r++
            }
            buf += P_i2(lya, mu, m, n, N, t, i) * prod
            i++
            prod = 0.0
        }
        buf
    }
}

fun S(lya: Double, mu: Double, m: Double, n: Double, N: Double, t: Double): Double {
    var buf = 0.0
    return if (m == N) {
        1 - lya.pow(N - n + 1) * (lya + mu).pow(-(N - n + 1))
    } else {
        var i = 0.0
        while (i < n) {
            buf += P_i2(lya, mu, m, n, N, t, i)
            i++
        }
        1 - buf
    }
}

fun U_(lya: Double, mu: Double, m: Double, n: Double, N: Double, t: Double): Double {
    var buf = 0.0
    var i = 0.0
    var prod = 0.0
    while (i < n) {
        var l = 0.0
        while (l < n - i) {
            prod += u_l(lya, mu, m, n, N, t, l, i)
            l++
        }
        buf += P_i2(lya, mu, m, n, N, t, i) * prod
        i++
        prod = 0.0
    }
    return 1 - buf
}

fun u_l(
    lya: Double,
    mu: Double,
    m: Double,
    n: Double,
    N: Double,
    t: Double,
    l: Double,
    i: Double
): Double {
    return (mu * t).pow(l) / fact(l) *
            (treg(N - i - m) * m.pow(l) * exp(-1 * i * mu * t) +
                    treg(m - N + i) * (N - i).pow(l) * exp(-1 * (N - i) * mu * t))
}

fun P_ir(
    lya: Double,
    mu: Double,
    m: Double,
    n: Double,
    N: Double,
    t: Double,
    r: Double,
    i: Double
): Double {
    return (i * lya * t).pow(r) / fact(r) * exp(-i * lya * t)
}

fun P_i2(
    lya: Double,
    mu: Double,
    m: Double,
    n: Double,
    N: Double,
    t: Double,
    j: Double
): Double {
    var temp = 0.0
    var i = 0
    while (i <= N) {
        temp += (mu / lya).pow(i.toDouble()) * (1 / fact(i.toDouble()))
        i++
    }
    return (mu / lya).pow(j) * (1.0 / fact(j)) * (1 / temp)
}

fun fact(n: Double): Double {
    return if (n < 2) 1.toDouble() else n * fact(n - 1)
}

fun treg(x: Double): Double {
    return if (x < 0) 0.toDouble() else 1.toDouble()
}