#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <algorithm>

using namespace std;

using Quad = struct {
    int height, width;
};

using Level = struct {
    vector<Quad> quad_list;
    int height{}, width{};
};

using HeightTime = pair<int, double>;

vector<double> list_value_ENFDH;
vector<double> list_value_EFFDH;

int compare(Quad x1, Quad x2) {
    return x1.height < x2.height;
}

vector<Quad> generate_task(int n, int count_task) {
    vector<Quad> quads;
    for (int i = 0; i < count_task; ++i) {
        Quad newquad = {0, 0};
        newquad.height = rand() % 99 + 1;
        newquad.width = rand() % (n - 1) + 1;
        quads.push_back(newquad);
    }
    return quads;
}

double calculate_t_t(vector<Quad> *all_quads, int count_n) {
    double result_sum = 0;
    for(size_t i = 0; i < all_quads->size(); ++i) {
        result_sum += (*all_quads)[i].height * (*all_quads)[i].width;
    }
    return 1.0 / count_n * result_sum;
}

double calculate_e(vector<Quad> *all_quads, double t_s, int count_n) {
    double t_t = calculate_t_t(all_quads, count_n);
    return (t_s - t_t) / t_t;
}

HeightTime NFDH(vector<Quad> *all_quads, int count_n) {
    vector<Level> level_list;

    int n = count_n;
    int current_level = 0;

    clock_t start = clock();
 
    Level new_level;

    new_level.height = (*all_quads)[0].height;
    new_level.width = (*all_quads)[0].width;

    level_list.push_back(new_level);

    for (size_t i = 1; i < all_quads->size(); ++i) {
        if (n - level_list[current_level].width >= (*all_quads)[i].width) {
            level_list[current_level].quad_list.push_back((*all_quads)[i]);

            level_list[current_level].width += (*all_quads)[i].width;
        } else {
            ++current_level;

            Level new_level;

            new_level.quad_list.push_back((*all_quads)[i]);

            new_level.width = (*all_quads)[i].width;
            new_level.height = (*all_quads)[i].height + level_list[current_level - 1].height;

            level_list.push_back(new_level);
        }
    }

    clock_t stop = clock();

    double e = calculate_e(all_quads, level_list[level_list.size() - 1].height, count_n);

    list_value_ENFDH.push_back(e);
    
    return HeightTime(level_list[level_list.size() - 1].height, double(stop - start) / CLOCKS_PER_SEC);
}

HeightTime FFDH(vector<Quad> *all_quads, int count_n) {
    vector<Level> level_list;

    int n = count_n;
    int current_level = 0;

    clock_t start = clock();
    
    Level new_level;

    new_level.height = (*all_quads)[0].height;
    new_level.width = (*all_quads)[0].width;

    level_list.push_back(new_level);
    
    bool is_added; 

    for (size_t i = 1; i < all_quads->size(); ++i) {
        is_added = false;
        for (auto & indexlevel : level_list) {
            if(n - indexlevel.width >= (*all_quads)[i].width) {
                indexlevel.quad_list.push_back((*all_quads)[i]);
                indexlevel.width += (*all_quads)[i].width;
                is_added = true;
                break;
            }
        }
        if (is_added) {
            continue;
        }
        if (n - level_list[current_level].width >= (*all_quads)[i].width) {
            level_list[current_level].quad_list.push_back((*all_quads)[i]);
            level_list[current_level].width += (*all_quads)[i].width;
        } else {
            ++current_level;
            Level new_level;
            new_level.quad_list.push_back((*all_quads)[i]);
            new_level.width = (*all_quads)[i].width;
            new_level.height = (*all_quads)[i].height + level_list[current_level - 1].height;
            level_list.push_back(new_level);
        }
    }

    clock_t stop = clock();

    double e = calculate_e(all_quads, level_list[level_list.size() - 1].height, count_n);

    list_value_EFFDH.push_back(e);

    return HeightTime(level_list[level_list.size() - 1].height, double(stop - start) / CLOCKS_PER_SEC);
}

double calculate_expected(vector<double> *value_list_e) {
    double result = 0.0;

    for(double i : *value_list_e) {
        result += i;
    }
    return result / value_list_e -> size();
}

double calculate_deviation(vector<double> *value_list_e) {
    double result = 0.0;

    double x = calculate_expected(value_list_e);

    for (double i : *value_list_e) {
        result += pow(i - x, 2);
    }

    return pow(result / (value_list_e -> size() - 1), 0.5);
}

int main(int argc, char **argv) {
    srand(time(nullptr));

    ofstream fout("output.txt");

    int tasks[] {500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000};

    for (int n : {1024, 4096}) {
        for (int task : tasks) {
            vector<Quad> all_quads = generate_task(n, task);

            sort(all_quads.begin(), all_quads.end(), compare);

            HeightTime resultNFDH = NFDH(&all_quads, n);
            HeightTime resultFFDH = FFDH(&all_quads, n);
            fout << resultNFDH.first << ";" << resultFFDH.first << ";" << resultNFDH.second << ";" << resultFFDH.second << endl;
        }
        fout << endl;

        fout << calculate_expected(&list_value_ENFDH) << ";" << calculate_deviation(&list_value_ENFDH) << endl;
        fout << calculate_expected(&list_value_EFFDH) << ";" << calculate_deviation(&list_value_EFFDH) << endl << endl;

        list_value_ENFDH.clear();
        list_value_EFFDH.clear();
    }
    fout.close();
    return 0;
}
