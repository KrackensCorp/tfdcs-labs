#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

double teta(double lambda, double mu, int n, int N, int m) {
    double sum = 0.0f;
    double multipy = 1.0f;
    double mu_l;

    for (int j = n + 1; j <= N; j++) {
        double l = j - 1;
        if (l >= N - m && l <= N) {
            mu_l = ((N - l) * mu);
        } else {
            mu_l = m * mu;
        }
        multipy *= mu_l / (l * lambda);
        sum += multipy / (j * lambda);
    }
    return sum + 1 / (n * lambda);
}

double t(double lambda, double mu, int n, int N, int m) {
    double mu_l;

    if (n == 1) {
        return m * mu;
    }

    double multipy = 1.0f;
    for (int l = 1; l <= n - 1; l++) {
        multipy *= (l * lambda) / (mu * l);
    }

    double sum = 0.0f;
    for (int j = 1; j <= n - 1; j++) {
        double multipy = 1.0f;
        for (int l = j; l <= n - 1; l++) {
            if (l >= N - m && l <= N) {
                mu_l = ((N - l) * mu);
            } else {
                mu_l = m * mu;
            }
            multipy *= l * lambda / mu_l;
        }
        sum += multipy / (j * lambda);
    }
    return multipy + sum;
}

double var2_1() {
    double lambda = 1e-5;
    int N = 65536;
    int m = 1;

    for (int n = 65526; n <= N; n++) {
        for (int mu = 1; mu <= 1000; mu *= 10) {
            cout << setw(0) << n << ";" << setw(15);
            for (int mu = 1; mu <= 1000; mu *= 10)
                cout << teta(lambda, mu, n, N, m) << ";" << setw(20);
            cout << endl;
        }
    }
    return 0;
}

double var2_2() {
    int N = 65536;
    int m = 1;
    double mu = 1.0;

    for (int n = 65526; n <= N; n++) {
        cout << setw(0) << n << ";" << setw(15);
        for (int i = -5; i >= -9; i--)
            cout << teta(pow(10.0, i), mu, n, N, m) << ";" << setw(20);
        cout << endl;
    }
    return 0;
}

double var2_3() {
    double lambda = 1e-5;
    int N = 65536;
    double mu = 1.0;

    for (int n = 65526; n <= N; n++) {
        cout << setw(0) << n << ";" << setw(15);
        for (int i = 1; i <= 4; i++)
            cout << teta(lambda, mu, n, N, i) << ";" << setw(20);
        cout << endl;
    }
    return 0;
}

double var3_1() {
    double lambda = 1e-3;
    int N = 1000;
    int m = 1;
    int mu[4] = {1, 2, 4, 6};

    for (int n = 900; n <= N; n += 10) {
        cout << setw(0) << n << ";" << setw(15);
        for (int i = 0; i < 4; i++) {
            cout << t(lambda, mu[i], n, N, m) << ";" << setw(20);
        }
        cout << endl;
    }
    return 0;
}

double var3_2() {
    int N = 8192;
    int m = 1;
    double mu = 1.0;

    for (int n = 8092; n <= N; n += 10) {
        cout << setw(0) << n << ";" << setw(15);
        for (int i = -5; i >= -9; i--) {
            cout << t(pow(10.0, i), mu, n, N, m) << ";" << setw(20);
        }
        cout << endl;
    }
    return 0;
}

double var3_3() {
    double lambda = 1e-5;
    int N = 8192;
    double mu = 1.0;

    for (int n = 8092; n <= N; n += 10) {
        cout << setw(0) << n << ";" << setw(15);
        for (int i = 1; i <= 4; i++) {
            cout << t(lambda, mu, n, N, i) << ";" << setw(20);
        }
        cout << endl;
    }
    return 0;
}

int main(int argc, char *argv[]) {
    int choice;

    cout << endl;
    cout << "1. (2.1) N = 65536; λ = 10^-5; m = 1; n = 65527, 65528, …, 65536; µ ∈ {1, 10, 100, 1000}.\n";
    cout << "2. (2.2) N = 65536; µ = 1; m = 1; n = 65527, 65528, …, 65536; λ ∈ {10^-5, 10^-6, 10^-7, 10^-8, 10^-9}.\n";
    cout << "3. (2.3) N = 65536; µ = 1; λ = 10^-5; n = 65527, 65528, …, 65536; m ∈ {1, 2, 3, 4}.\n";
    cout << "4. (3.1) N = 1000; λ = 10^-3; m = 1; n = 900, 910, …, 1000; µ ∈ {1, 2, 4, 6}.\n";
    cout << "5. (3.2) N = 8192; µ = 1; m = 1; n = 8092, 8102, …, 8192; λ ∈ {10^-5, 10^-6, 10^-7, 10^-8, 10^-9}.\n";
    cout << "6. (3.3) N = 8192; µ = 1; λ = 10^-5; n = 8092, 8102, …, 8192; m ∈ {1, 2, 3, 4}.\n";
    cout << endl;
    cout << "Select: ";

    cin >> choice;
    if (choice == 1) return var2_1();
    if (choice == 2) return var2_2();
    if (choice == 3) return var2_3();
    if (choice == 4) return var3_1();
    if (choice == 5) return var3_2();
    if (choice == 6) return var3_3();
}
